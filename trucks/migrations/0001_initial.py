# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('lon', models.DecimalField(max_digits=8, decimal_places=5)),
                ('lat', models.DecimalField(max_digits=8, decimal_places=5)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
