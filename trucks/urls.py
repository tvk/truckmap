from django.conf.urls import patterns, url

urlpatterns = patterns('trucks.views',
    url(r'^$', 'index', name='trucks-index'),
)
