from django.db import models

class Location(models.Model):
	name = models.CharField(max_length=200)
	lon  = models.DecimalField(max_digits=8, decimal_places=5)
	lat  = models.DecimalField(max_digits=8, decimal_places=5) 
