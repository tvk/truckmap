#from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext, loader

from .models import Location

def index(request):
    template = loader.get_template('trucks/index.html')
    context = RequestContext(request, {
        'locs': Location.objects.order_by('name'),
    })
    return HttpResponse(template.render(context))
